﻿using Serilog;
using System;

namespace GroupPolicyGuard;

public class Service : IDisposable
{
    private readonly ILogger _log;

    private readonly PolicyWatcher _policyWatcher;

    public Service(ILogger log)
    {
        _log = log;
        _policyWatcher = new PolicyWatcher(Config.PolicyValues, log);
    }

    public void Dispose()
    {
        _policyWatcher?.Dispose();
    }

    public void Start()
    {
        _log.Information("Starting group policy watcher");
        _policyWatcher.Start();
        _log.Information("Started group policy watcher");
    }

    public void Stop()
    {
        _log.Information("Stopping group policy watcher");
        _policyWatcher.Stop();
        _log.Information("Stopped group policy watcher");
    }
}
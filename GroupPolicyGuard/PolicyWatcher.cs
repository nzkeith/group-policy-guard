﻿using Microsoft.Win32;
using RegistryUtils;
using Serilog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reactive.Linq;
using System.Threading;

namespace GroupPolicyGuard;

public class PolicyWatcher : IDisposable
{
    private static readonly TimeSpan ScheduledCheckPeriod = TimeSpan.FromMinutes(10);
    private static readonly TimeSpan RegistryChangeDebounceInterval = TimeSpan.FromSeconds(2);

    private static readonly object DefaultRegistryValue = new ();

    private readonly IReadOnlyCollection<PolicyValue> _policyValues;
    private readonly ILogger _log;

    private readonly RegistryMonitor _registryMonitor;
    private readonly Timer _timer;
    private readonly IDisposable _registryChangeSubscription;

    public PolicyWatcher(IReadOnlyCollection<PolicyValue> policyValues, ILogger log)
    {
        _policyValues = policyValues ?? throw new ArgumentNullException(nameof(policyValues));
        _log = log ?? throw new ArgumentNullException(nameof(log));

        _registryMonitor = new RegistryMonitor(RegistryHive.LocalMachine, PolicyValue.PoliciesRegistryKey)
        {
            RegChangeNotifyFilter = RegChangeNotifyFilter.Key | RegChangeNotifyFilter.Value
        };
        _registryMonitor.Error += OnRegistryMonitorError;

        _registryChangeSubscription = Observable
            .FromEventPattern<EventHandler, EventArgs>(
                handler => _registryMonitor.RegChanged += handler,
                handler => _registryMonitor.RegChanged -= handler)
            .Throttle(RegistryChangeDebounceInterval)
            .Subscribe(e => OnRegistryChange(e.Sender, e.EventArgs));

        _timer = new Timer(RunScheduledCheck);
    }

    public void Dispose()
    {
        _timer?.Dispose();
        _registryChangeSubscription?.Dispose();
        _registryMonitor?.Dispose();
    }

    public void Start()
    {
        _log.Information("Starting registry monitor");
        _registryMonitor.Start();
        _log.Information("Started registry monitor");

        _log.Information("Starting scheduled check timer");
        _timer.Change(TimeSpan.FromSeconds(5), ScheduledCheckPeriod);
        _log.Information("Started scheduled check timer");
    }

    public void Stop()
    {
        _log.Information("Stopping scheduled check timer");
        _timer.Change(Timeout.Infinite, Timeout.Infinite);
        _log.Information("Timer stopped");

        _log.Information("Stopping registry monitor");
        _registryMonitor.Stop();
        _log.Information("Registry monitor stopped");
    }

    private void OnRegistryChange(object sender, EventArgs args)
    {
        _log.Information("Detected registry key change");
        EnforcePolicyValues();
    }

    private void OnRegistryMonitorError(object sender, ErrorEventArgs args)
    {
        _log.Error(args.GetException(), "Registry monitor threw an exception");
    }

    private void RunScheduledCheck(object _)
    {
        _log.Debug("Running scheduled check");
        EnforcePolicyValues();
    }

    private void EnforcePolicyValues()
    {
        _log.Information("Checking policy values");
        foreach (var policyValue in _policyValues)
        {
            try
            {
                _log.Debug($"Checking {policyValue}");
                EnforcePolicyValue(policyValue);
            }
            catch (Exception ex)
            {
                _log.Error(ex, $"Exception thrown while checking {policyValue}");
            }
        }
    }

    private void EnforcePolicyValue(PolicyValue policyValue)
    {
        if (HasDesiredValue(policyValue))
        {
            return;
        }

        PolicyUpdater.UpdateGroupPolicy(policyValue, _log);
    }

    private bool HasDesiredValue(PolicyValue policyValue)
    {
        const int tries = 5;
        var tryCount = 0;
        while (true)
        {
            var registryValue = Registry.GetValue(
                $@"{PolicyValue.PoliciesRegistryHive}\{policyValue.FullKey}",
                policyValue.ValueName,
                DefaultRegistryValue);

            if (registryValue == null)
            {
                throw new Exception($"Registry key not found for {policyValue}");
            }

            if (registryValue != DefaultRegistryValue)
            {
                var hasDesiredValue = StructuralComparisons.StructuralEqualityComparer.Equals(policyValue.Value, registryValue);
                if (!hasDesiredValue)
                {
                    _log.Information($"Detected undesired value for {policyValue} = {PolicyValue.ValueToString(registryValue)}");
                }
                return hasDesiredValue;
            }

            tryCount++;
            if (tryCount >= tries)
            {
                throw new Exception($"Registry value not found for {policyValue}");
            }

            _log.Information($"Name not found in specified key for {policyValue} - retrying shortly");
            Thread.Sleep(1000);
        }
    }
}
﻿using LocalPolicy;
using System;
using System.Threading;
using Serilog;

namespace GroupPolicyGuard;

public static class PolicyUpdater
{
    private static readonly GroupPolicySection GroupPolicySection = GroupPolicySection.Machine;

    public static void UpdateGroupPolicy(PolicyValue policyValue, ILogger log)
    {
        log.Debug($"Starting group policy update thread for {policyValue}");
        var thread = new Thread(() => UpdateGroupPolicyInternal(policyValue, log));
        thread.SetApartmentState(ApartmentState.STA);
        thread.Start();
        thread.Join();
        log.Debug($"Group policy update thread completed for {policyValue}");
    }

    private static void UpdateGroupPolicyInternal(PolicyValue policyValue, ILogger log)
    {
        log.Debug($"Updating group policy value {policyValue}");
        try
        {
            log.Debug($"Opening registry subkey {GroupPolicySection}:{policyValue.FullKey}");
            var gpo = new ComputerGroupPolicyObject();
            using var machine = gpo.GetRootRegistryKey(GroupPolicySection);
            using var key = machine.CreateSubKey(policyValue.FullKey);
            if (key == null)
            {
                throw new Exception($"CreateSubKey failed for {GroupPolicySection}:{policyValue.FullKey}");
            }

            log.Debug($"Setting {policyValue.ValueName} to {policyValue.Value}");
            key.SetValue(policyValue.ValueName, policyValue.Value, policyValue.ValueKind);

            log.Debug($"Closing registry subkey {GroupPolicySection}:{policyValue.FullKey}");
            key.Close();

            log.Debug("Saving group policy object");
            gpo.Save();

            log.Information($"Updated group policy value {policyValue}");
        }
        catch (Exception ex)
        {
            log.Error(ex, $"Exception thrown while updating group policy value {policyValue}");
        }
    }
}
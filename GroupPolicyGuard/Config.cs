﻿using Microsoft.Win32;
using Serilog;

namespace GroupPolicyGuard;

public static class Config
{
    public static LoggerConfiguration LoggerConfiguration = new LoggerConfiguration()
        .MinimumLevel.Information();

    public static readonly PolicyValue[] PolicyValues =
    {
        new (
            key: @"Microsoft\Windows Defender",
            valueName: "DisableLocalAdminMerge",
            valueKind: RegistryValueKind.DWord,
            value: 0),
        new (
            key: @"Microsoft\Windows Defender\real-time protection",
            valueName: "DisableRealtimeMonitoring",
            valueKind: RegistryValueKind.DWord,
            value: 1)
    };
}
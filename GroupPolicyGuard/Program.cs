﻿using System;
using Serilog;
using Topshelf;

namespace GroupPolicyGuard
{
    public static class Program
    {
        private const string LogOutputTemplate = "[{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} {Level:u3}] {Message:lj}{NewLine}{Exception}";
        private const long LogFileSizeLimitBytes = 1L * 10 * 1024 * 1024;
        private const int LogRetainedFileCountLimit = 5;
        private static readonly string LogPath =
            $@"{Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)}\GroupPolicyGuard\Logs\log.txt";

        public static void Main()
        {
            var loggerConfiguration = Config.LoggerConfiguration;
            if (Environment.UserInteractive)
            {
                loggerConfiguration.WriteTo.Console(outputTemplate: LogOutputTemplate);
            }
            else
            {
                loggerConfiguration.WriteTo.File(LogPath, outputTemplate: LogOutputTemplate,
                    rollOnFileSizeLimit: true, fileSizeLimitBytes: LogFileSizeLimitBytes, retainedFileCountLimit: LogRetainedFileCountLimit);
            }

            var log = loggerConfiguration.CreateLogger();

            var rc = HostFactory.Run(x =>
            {
                x.Service<Service>(s =>
                {
                    s.ConstructUsing(_ => new Service(log));
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("Group Policy Guard");
                x.SetDisplayName("Group Policy Guard");
                x.SetServiceName("GroupPolicyGuard");

                x.EnableServiceRecovery(r =>
                {
                    r.RestartService(delayInMinutes: 1);
                    r.RestartService(delayInMinutes: 2);
                    r.RestartService(delayInMinutes: 5);
                    r.SetResetPeriod(days: 1);
                });

                x.UseSerilog(log);
            });

            var exitCode = (int)Convert.ChangeType(rc, rc.GetTypeCode());
            Environment.ExitCode = exitCode;
        }
    }
}

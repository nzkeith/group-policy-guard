﻿using System;
using Microsoft.Win32;

namespace GroupPolicyGuard;

public class PolicyValue
{
    public const string PoliciesRegistryHive = "HKEY_LOCAL_MACHINE";
    public const string PoliciesRegistryKey = @"SOFTWARE\Policies";

    public PolicyValue(string key, string valueName, RegistryValueKind valueKind, object value)
    {
        if (string.IsNullOrWhiteSpace(key))
        {
            throw new ArgumentException($"{nameof(key)} cannot be null or whitespace.", nameof(key));
        }
        if (string.IsNullOrWhiteSpace(valueName))
        {
            throw new ArgumentException($"{nameof(valueName)} cannot be null or whitespace.", nameof(valueName));
        }

        Key = key;
        ValueName = valueName;
        ValueKind = valueKind;
        Value = value ?? throw new ArgumentNullException(nameof(value));
    }

    private string Key { get; }
    public string ValueName { get; }
    public RegistryValueKind ValueKind { get; }
    public object Value { get; }

    public string FullKey => $@"{PoliciesRegistryKey}\{Key}";

    public override string ToString() => $@"[{Key}\{ValueName}:{ValueToString(Value)}]";

    public static string ValueToString(object value)
        => value is Array ? string.Join(",", value) : value.ToString();
}
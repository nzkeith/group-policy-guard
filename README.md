# Group Policy Guard

## Install

Build the solution's `Release` configuration, then run the following as Administrator:

```
.\GroupPolicyGuard\bin\Release\net6.0-windows\GroupPolicyGuard.exe install
.\GroupPolicyGuard\bin\Release\net6.0-windows\GroupPolicyGuard.exe start
```

The service runs using the local system account. Log files are stored in `%ProgramData%\GroupPolicyGuard\Logs`.

## Uninstall

Build the solution's `Release` configuration, then run the following as Administrator:

```
.\GroupPolicyGuard\bin\Release\net6.0-windows\GroupPolicyGuard.exe stop
.\GroupPolicyGuard\bin\Release\net6.0-windows\GroupPolicyGuard.exe uninstall
```

## Development

Build and run the solution's 'Debug' configuration. If the service is installed, you'll need to stop it temporarily.
